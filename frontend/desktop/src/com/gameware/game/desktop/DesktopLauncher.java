package com.gameware.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.gameware.game.GameWare;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = GameWare.WIDTH;
		config.height = GameWare.HEIGHT;
		config.title = GameWare.TITLE;
		new LwjglApplication(GameWare.getInstance(), config);
	}
}
