package com.gameware.game.models;

import com.badlogic.gdx.utils.Json;
import com.gameware.game.QueryIntermediate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/*
    Model for tournaments.
*/

public class Tournament implements ModelInterface {
    private String _id;
    private List<String> players;
    private List<String> games;
    private String name;
    private double timePerRound;
    private int maxPlayers;
    private int roundsPerGame;
    private int currentRound;
    private String dateCreated;
    private boolean active;
    private int currentPlayers;
    private int totalGames;
    private int startDelay;
    private String startTime;

    public Tournament() {
    }

    public Tournament(String playerId, List<String> games, String name, double timePerRound, int maxPlayers,
            int roundsPerGame, int currentPlayers, int totalGames, int startDelay) {
        this.players = new ArrayList<>();
        this.players.add(playerId);
        this.games = games;
        this.name = name;
        this.timePerRound = timePerRound;
        this.maxPlayers = maxPlayers;
        this.roundsPerGame = roundsPerGame;
        this.currentPlayers = currentPlayers;
        this.totalGames = totalGames;
        this.startDelay = startDelay;
    }

    public Tournament(String playerId, List<String> games, String name, double timePerRound, int maxPlayers,
                      int roundsPerGame, int currentPlayers, int totalGames, int startDelay, String startTime) {
        this.players = new ArrayList<>();
        this.players.add(playerId);
        this.games = games;
        this.name = name;
        this.timePerRound = timePerRound;
        this.maxPlayers = maxPlayers;
        this.roundsPerGame = roundsPerGame;
        this.currentPlayers = currentPlayers;
        this.totalGames = totalGames;
        this.startDelay = startDelay;
        this.startTime = startTime;
    }



    public String get_id() {
        return _id;
    }

    public List<String> getPlayers() {
        return players;
    }

    public int getStartDelay() {
        return startDelay;
    }

    public List<String> getGames() {
        return games;
    }

    public String getName() {
        return name;
    }

    public double getTimePerRound() {
        return timePerRound;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public int getRoundsPerGame() {
        return roundsPerGame;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public Date getDateCreated() {
        return QueryIntermediate.convertToDate(dateCreated);
    }

    public boolean isActive() {
        return active;
    }

    public int getTotalGames() {
        return totalGames;
    }

    public Date getStartTime() {
        return QueryIntermediate.convertToDate(startTime);
    }

    public int getCurrentPlayers() {
        return currentPlayers;
    }

    public void reset() {
        _id = null;
        players = null;
        games = null;
        name = null;
        timePerRound = Double.parseDouble(null);
        maxPlayers = Integer.parseInt(null);
        roundsPerGame = Integer.parseInt(null);
        currentRound = Integer.parseInt(null);
        dateCreated = null;
        active = Boolean.parseBoolean(null);
    }

    public String report() {
        return new Json().toJson(this);
    }

    @Override
    public String toString() {
        return report();
    }
}
