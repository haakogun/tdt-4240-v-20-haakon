package com.gameware.game.models;

import com.badlogic.gdx.utils.Json;

/*
    Model for the local storage. Used to remember log in and options.
    Loads in the config.json file, placed in the package "assets" under the package "android"
*/

public class LocalStorage implements ModelInterface {
    private Player player;
    private Boolean musicOn;
    private Boolean soundEffects;
    private Boolean includeFin;

    public LocalStorage() {}

    public LocalStorage(Player player, Boolean musicOn, Boolean soundEffects, Boolean includeFin) {
        this.player = player;
        this.musicOn = musicOn;
        this.soundEffects = soundEffects;
        this.includeFin = includeFin;
    }

    public Player getPlayer() {
        return player;
    }

    public Boolean getMusicOn() {
        return musicOn;
    }

    public Boolean getSoundEffects() {
        return soundEffects;
    }

    public Boolean getIncludeFin() {
        return includeFin;
    }

    @Override
    public void reset() {
        player = null;
        musicOn = true; // default value
        soundEffects = true; // default value
        includeFin = false; // default value
    }

    @Override
    public String report() {
        Json json = new Json();
        json.setUsePrototypes(false);
        return json.toJson(this);
    }

    @Override
    public String toString() {
        return report();
    }
}
