package com.gameware.game.models;

import com.badlogic.gdx.utils.Json;
import com.gameware.game.QueryIntermediate;

import java.util.Date;

/*
    Model for a player's round in a specific tournament.
*/

public class Round implements ModelInterface {
    private String _id;
    private String tournamentId;
    private String playerId;
    private String gameId;
    private int roundNr;
    private String deadlineDate;
    private double scoreValue;
    private boolean hasPlayed;
    private int tournamentPoints;

    public Round() {
    }

    public String get_id() { return _id; }

    public String getTournamentId() { return tournamentId;}

    public String getPlayerId() { return playerId; }

    public String getGameId() { return gameId; }

    public int getRoundNr() { return roundNr; }

    public Date getDeadlineDate() { return QueryIntermediate.convertToDate(deadlineDate); }

    public double getScoreValue() { return scoreValue; }

    public boolean isPlayed() { return hasPlayed; }

    public int getTournamentPoints() { return tournamentPoints; }

    public void reset() {
        _id = null;
        tournamentId = null;
        playerId = null;
        gameId = null;
        roundNr = Integer.parseInt(null);
        deadlineDate = null;
        scoreValue = Double.parseDouble(null);
        hasPlayed = Boolean.parseBoolean(null);
        tournamentPoints = Integer.parseInt(null);
    }

    public String report() {
        return new Json().toJson(this);
    }

    @Override
    public String toString() {
        return report();
    }
}
