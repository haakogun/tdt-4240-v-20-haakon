package com.gameware.game.models;

import com.badlogic.gdx.utils.Json;
import com.gameware.game.QueryIntermediate;

import java.util.Date;
import java.util.jar.JarEntry;

/*
    Model for users. The Player variable in the GameWare is set when logging in and out
*/

public class Player implements ModelInterface {
    private String _id;
    private String name;
    private String password;
    private String dateJoined;

    public Player(String _id, String name) {
        this._id = _id;
        this.name = name;
    }

    public Player() {}

    public String getId() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public Date getDateJoined() {
        return QueryIntermediate.convertToDate(dateJoined);
    }

    public void reset() {
        this._id = null;
        this.name = null;
    }

    public String report() {
        return new Json().toJson(this);
    }

    @Override
    public String toString() {
        return report();
    }
}
