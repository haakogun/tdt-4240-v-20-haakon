package com.gameware.game.models;

import com.badlogic.gdx.utils.Json;

/*
    Model for games
*/

public class Game implements ModelInterface {
    private String _id;
    private String name;
    private String explanation;

    public Game(String _id, String name, String explanation) {
        this._id = _id;
        this.name = name;
        this.explanation = explanation;
    }

    public Game() {
    }

    public String getId() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public String getExplanation() {
        return explanation;
    }

    @Override
    public String toString() {
        return report();
    }

    public void reset() {
        this._id = null;
        this.name = null;
    }

    public String report() {
        return new Json().toJson(this);
    }

}
