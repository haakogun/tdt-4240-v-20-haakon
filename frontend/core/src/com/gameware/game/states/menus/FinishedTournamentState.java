package com.gameware.game.states.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gameware.game.GameWare;
import com.gameware.game.QueryIntermediate;
import com.gameware.game.models.Point;
import com.gameware.game.models.Tournament;
import com.gameware.game.GameStateManager;

import java.util.ArrayList;
import java.util.List;

/*
    State where players can view the tournament results.

    Patterns: Publish-Subscribe, Delegation

    Publish-Subscribe and delegation is used on the buttons. Where P-S is used by utilizing
    click listeners and delegation is used by the button to delegate what happens to this state.
*/

public class FinishedTournamentState extends MenuStateUnion {

//    Data
    private Tournament tournament;
    private List<Point> points = new ArrayList<>();

//    Labels
    private Label tournamentNameLabel = new Label("N/A", skin,"big");
    private final Label finishedLabel = new Label("Final results", skin,"big");
    private final Label colOneLabel = new Label("Player", skin, "big");
    private final Label colTwoLabel = new Label("Total", skin, "big");
    private final Label noRoundsPlayedLabel = new Label("No rounds played", skin);

//    Texts
    private final String backBtnText = "Back";
    private final String leaveBtnText = "Leave";
    private final String winnerText = "Winner: ";
    private final String leaveDialogText = "Are you sure want to\nleave ";

//    Variables
    private final Color scrollPaneBGColor = Color.GOLD;

    private Dialog dialog;


    public FinishedTournamentState(GameStateManager gsm, Tournament tournament) {
        super(gsm);
        this.tournament = tournament;
        try {
            tournamentNameLabel.setText(tournament.getName());
            points = QueryIntermediate.getTournamentPoints(tournament.get_id());
        }catch(Exception e){
            e.printStackTrace();
        }
        makeStage();
    }

    protected void makeStage(){
        Table rootTable = makeRootTable(backgroundFinTourn);
        rootTable.defaults().spaceBottom(spacingMedium);

        tournamentNameLabel.setFontScale(tinierTitleFontBigScale);
        rootTable.add(tournamentNameLabel);
        rootTable.row();

        rootTable.add(finishedLabel);
        rootTable.row();

        if(!points.isEmpty()) {
            rootTable.add(makeHSTable());
            rootTable.row();

            String winnerName = points.get(0).getName();
            rootTable.add(new Label(winnerText + winnerName, skin, "big")).spaceBottom(spacingLittle);
            rootTable.row();
        }else{
            rootTable.add(noRoundsPlayedLabel);
        }

        Table btnTable = new Table();
        btnTable.add(makeBackBtn()).spaceRight(spacingLittle);
        TextButton leaveBtn = new TextButton(leaveBtnText, skin);
        leaveBtn.addListener(new LeaveClickListener());
        btnTable.add(leaveBtn);
        rootTable.add(btnTable);

        stage.addActor(rootTable);

        makeLeaveDialog();
    }

//    Make widgets methods
    private Table makeHSTable(){
        Table hsTable = new Table();
        hsTable.pad(padding);
        hsTable.defaults().space(spacingLittle);
        hsTable.setBackground(makeColorBackground(scrollPaneBGColor));

        hsTable.add(colOneLabel);
        hsTable.add(colTwoLabel);
        hsTable.row();

        for(Point point : points){
            hsTable.add(new Label(point.getName(), skin));
            hsTable.add(new Label(point.getTotalPoints()+"", skin));
            hsTable.row();
        }
        return hsTable;
    }

    private void makeLeaveDialog(){
        dialog = new Dialog("", skin, "dialog") {
            public void result(Object obj) {
                if(obj.equals(true)){
                    try {
                        QueryIntermediate.leaveTournament(tournament.get_id(), GameWare.getInstance().getPlayer().getId());
                        CreateJoinTournamentState state = new CreateJoinTournamentState(gsm);
                        state.setFeedbackLabelText(state.leftTournamentText+tournament.getName());
                        gsm.set(state);
                    } catch(Exception e){
                        System.out.println(e);
                    }
                } else{
                    makeLeaveDialog();
                }

            }
        };
        Label l = new Label(leaveDialogText+tournament.getName()+"?\n", skin);
        l.setAlignment(Align.center);
        dialog.text(l);
        dialog.button("Yes", true).pad(padding); //sends "true" as the result
        dialog.button("No", false).pad(padding);  //sends "false" as the result
    }

    private TextButton makeBackBtn(){
        TextButton backBtn = new TextButton(backBtnText, makeTextButtonStyle(backColor));
        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                handleBackBtnClick();
            }
        });
        return backBtn;
    }

//    Handle click methods
    private void handleBackBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new CreateJoinTournamentState(gsm));
    }

    private void handleLeaveBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        dialog.show(stage);
    }

    public class LeaveClickListener extends ClickListener{
        public void clicked(InputEvent event, float x, float y) {
            handleLeaveBtnClick();
        };
    }

    @Override
    protected void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)){
            handleBackBtnClick();
        }
    }
}
