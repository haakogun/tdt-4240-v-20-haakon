package com.gameware.game.states.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Scaling;
import com.gameware.game.GameWare;
import com.gameware.game.QueryIntermediate;
import com.gameware.game.models.Game;
import com.gameware.game.models.Round;
import com.gameware.game.models.Tournament;
import com.gameware.game.sprites.LoadingText;
import com.gameware.game.GameStateManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
    State where players can create new tournaments. This state contains two pages, to not
    many unnecessary many states.

    Patterns: Publish-Subscribe, Delegation

    Publish-Subscribe and delegation is used on the buttons. Where P-S is used by utilizing
    click listeners and delegation is used by the button to delegate what happens to this state.
*/

public class CreateNewTournamentState extends MenuStateUnion {

//    Data
    private List<Game> games;
    private Tournament tournament;
    private ArrayList<CheckBox> checkBoxes = new ArrayList<>();
    private List<String> chosenGames = new ArrayList<>();
    private Round round;
    private Tournament tournamentFromDB;

//    Lables
    private final Label titleLabel = new Label("Create new\ntournament", skin, "big");
    private final Label subHeadLabel = new Label("Select games", skin, "big");
    private final Label errorLabel = new Label("", skin, "error");

    private final Label nameLabel = new Label("Name*", skin);
    private final Label timePerRoundLabel = new Label("Days per round", skin);
    private final Label maxPlayersLabel = new Label("Max players", skin);
    private final Label roundsPerGameLabel = new Label("Rounds per game", skin);

    private final Label nameLabelQuestion = new Label("Name of the tournament. This will be shown to other players", skin);
    private final Label timePerRoundLabelQuestion = new Label("Number of days that players have to finish their own turn in the tournament. If they don't finish within the dealine they are kicked out of the tournament.\n\nEven if max number of players has not been reached, the tournament still starts after specified number of days.", skin);
    private final Label maxPlayersLabelQuestion = new Label("Max number of players able to enter the tournament. If max number of players enter and play their round, then the tournament continues.", skin);
    private final Label roundsPerGameLabelQuestion = new Label("Number of rounds per game. For example if you choose 2 games, and have 3 rounds per game then you will play each game 3 times.", skin);

//    Texts
    private final String backBtnText = "Back";
    private final String createBtnText = "Create";
    private final String nextBtnText = "Select games";

    private final String noNameText = "No name given";
    private final String noGamesSelectedText = "No games selected";
    private final String IOExceptionText = "Something went wrong when\ncreating the tournament";


//    Input fields
    private TextField nameInputField = new TextField("",skin);
    private SelectBox timePerRoundSelectBox = new SelectBox(skin);
    private SelectBox maxPlayersSelectBox = new SelectBox(skin);
    private SelectBox roundsPerGameSelectBox = new SelectBox(skin);

//    User inputs
    private String nameUserInput = "";
    private int timePerRoundUserInput = 1;
    private int maxPlayersUserInput = 2;
    private int roundsPerGameUserInput = 1;

//    Variables
    private int page = 1;
    private final int inputFieldHeight = Gdx.graphics.getHeight()/15;
    private final int nameFieldWidth = Gdx.graphics.getWidth()/3;
    private final float checkBoxSize = Gdx.graphics.getWidth()/15;
    private final float scrollPaneWidth = Gdx.graphics.getWidth()/1.14f;
    private final float scrollPaneHeight = Gdx.graphics.getHeight()/2.3f;

    private Table rootTable;


//    Dialogs
    private Dialog nameDialog;
    private Dialog timePerRoundDialog;
    private Dialog maxPlayersDialog;
    private Dialog roundsPerGameDialog;

//    Loading text
    private LoadingText loadingText = new LoadingText();
    private boolean backBtnClicked = false;
    private boolean createBtnClicked = false;


    public CreateNewTournamentState(GameStateManager gsm) {
        super(gsm);
        try {
            games = GameWare.getInstance().getGames();
        } catch (IOException e) {
            e.printStackTrace();
        }

        timePerRoundSelectBox.setItems(1,2,3,4,5);
        maxPlayersSelectBox.setItems(2,3,4,5);
        roundsPerGameSelectBox.setItems(1,2,3);

        nameDialog = makeQuestionIconDialog(nameLabelQuestion);
        timePerRoundDialog = makeQuestionIconDialog(timePerRoundLabelQuestion);
        maxPlayersDialog = makeQuestionIconDialog(maxPlayersLabelQuestion);
        roundsPerGameDialog = makeQuestionIconDialog(roundsPerGameLabelQuestion);

        makeStage();
    }

    protected void makeStage(){
        rootTable = makeRootTable();

        titleLabel.setFontScale(titleFontBigScale);
        rootTable.add(titleLabel).colspan(2).expandY().top();
        rootTable.row();

        if(page == 1){
            makePageOne();
        }else if(page == 2){
            makePageTwo();
        }
        stage.addActor(rootTable);
    }

//    Make widgets methods
    private void makePageOne(){
        Table innerTable = new Table();
        innerTable.defaults().spaceBottom(spacingLittle*1.5f);
        innerTable.pad(spacingLittle);
        innerTable.setBackground(backgroundTableBlueRounded);

        innerTable.add(makeTableWithLabelAndQuestionIcon(nameLabel, nameDialog));
        nameInputField.setMaxLength(9);
        innerTable.add(nameInputField).size(nameFieldWidth, inputFieldHeight);
        innerTable.row();
        removeKeyPadAtTouch();
        innerTable.add(makeTableWithLabelAndQuestionIcon(maxPlayersLabel, maxPlayersDialog));
        innerTable.add(maxPlayersSelectBox);
        innerTable.row();
        innerTable.add(makeTableWithLabelAndQuestionIcon(roundsPerGameLabel, roundsPerGameDialog));
        innerTable.add(roundsPerGameSelectBox);
        innerTable.row();
        innerTable.add(makeTableWithLabelAndQuestionIcon(timePerRoundLabel, timePerRoundDialog));
        innerTable.add(timePerRoundSelectBox);
        rootTable.add(innerTable).colspan(2);
        rootTable.row();
        rootTable.add(errorLabel).colspan(2);
        rootTable.row();
        rootTable.add(makeBackBtn()).expand().bottom().left();

        TextButton nextBtn = makeNextBtn();
        rootTable.add(nextBtn).size(buttonWidth*1.2f, buttonHeight).expand().bottom().right();
    }

    private void makePageTwo(){
        rootTable.add(subHeadLabel).colspan(2);
        rootTable.row();

        Table innerTable = new Table();
        innerTable.pad(spacingLittle);
        innerTable.defaults().space(spacingLittle);
        innerTable.setBackground(backgroundTableBlueRounded);

        for (final Game g : games){
            if(GameWare.getInstance().getGameIdToPlayState().get(g.getId()) == null){
                continue;
            }

            innerTable.add(new Image(GameWare.getInstance().getGameIdToPlayState().get(g.getId()).getThumbnail())).width(imageWidthAndHeigh).height(imageWidthAndHeigh);

            Table innerInnerTable = new Table();
            innerInnerTable.defaults().space(spacingLittle);
            innerInnerTable.add(makeTableWithLabelAndQuestionIcon(new Label(g.getName(), skin), makeQuestionIconDialog(new Label(g.getExplanation().replaceAll("\\\\n", "\n"), skin)))).spaceBottom(spacingLittle);
            innerInnerTable.row();

            CheckBox gameCB = new CheckBox("",skin);
            gameCB.addListener(new ClickListener(){
                @Override
                public void clicked(InputEvent e, float x, float y){
                    if(GameWare.getInstance().isSoundEffectsOn()){ checkBoxSound.play(); }
                    if(chosenGames.contains(g.getId())){
                        chosenGames.remove(g.getId());
                    }
                    else{
                        chosenGames.add(g.getId());
                    }
                }
            });
            if(chosenGames.contains(g.getId())){
                gameCB.setChecked(true);
            }
            gameCB.getImage().setScaling(Scaling.fill);
            gameCB.getImageCell().size(checkBoxSize);
            checkBoxes.add(gameCB);
            innerInnerTable.add(gameCB);

            innerTable.add(innerInnerTable);
            innerTable.row();
        }

        ScrollPane scrollPane = new ScrollPane(innerTable, skin);
        rootTable.add(scrollPane).colspan(2);
        rootTable.getCell(scrollPane).size(scrollPaneWidth, scrollPaneHeight);
        rootTable.row();

        rootTable.add(errorLabel).colspan(2);
        rootTable.row();

        rootTable.add(makeBackBtn()).expand().bottom().left();

        TextButton createBtn = makeCreateBtn();
        rootTable.add(createBtn).size(buttonWidth, buttonHeight).expand().bottom().right();

        stage.addActor(rootTable);
    }

    private TextButton makeCreateBtn() {
        TextButton createBtn = new TextButton(createBtnText, makeTextButtonStyle(greenColor));
        createBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                setCreateBtnClicked();
            }
        });
        return createBtn;
    }

    private TextButton makeNextBtn() {
        TextButton nextBtn = new TextButton(nextBtnText, skin);
        nextBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }

                if(getTournamentName().equals("")){
                    errorLabel.setText(noNameText);
                    return;
                }

                page ++;

                errorLabel.setText("");
                nameUserInput = getTournamentName();
                timePerRoundUserInput = getTimePerRound();
                maxPlayersUserInput = getMaxPlayers();
                roundsPerGameUserInput = getRoundsPerGame();

                stage.clear();
                makeStage();
            }
        });
        return nextBtn;
    }

    private TextButton makeBackBtn() {
        TextButton backBtn = new TextButton(backBtnText, makeTextButtonStyle(backColor));
        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                setBackBtnClicked();
            }
        });
        return backBtn;
    }

    @Override
    public void update(float dt) {
        super.update(dt);

        if(this.backBtnClicked && this.loadingText.textIsRendering()){
            this.handleBackBtnClick();
            this.backBtnClicked = false;
        }

        if(this.createBtnClicked && this.loadingText.textIsRendering()){
            this.handleCreateBtnClick();
            this.createBtnClicked = false;
        }

        this.loadingText.update(dt);
    }

    @Override
    public void render(SpriteBatch sb) {
        super.render(sb);

        this.loadingText.draw(sb);
    }

    public void setBackBtnClicked(){
        if(page == 1) {
            this.backBtnClicked = true;
            this.loadingText.setLoading();
        }
        else{
            this.handleBackBtnClick();
        }
    }

    public void setCreateBtnClicked(){
        this.createBtnClicked = true;
        this.loadingText.setLoading();
    }

//    Handle click methods
    private void handleBackBtnClick() {
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        if(page == 1) {
            gsm.set(new CreateJoinTournamentState(gsm));
        }else if (page == 2){
            page --;

            stage.clear();
            makeStage();

            errorLabel.setText("");
            nameInputField.setText(nameUserInput);
            timePerRoundSelectBox.setSelected(timePerRoundUserInput);
            maxPlayersSelectBox.setSelected(maxPlayersUserInput);
            roundsPerGameSelectBox.setSelected(roundsPerGameUserInput);
        }
    }

    private void handleCreateBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        if(chosenGames.isEmpty()){
            errorLabel.setText(noGamesSelectedText);
            return;
        }
        try {
            tournament = new Tournament(GameWare.getInstance().getPlayer().getId(),chosenGames,nameUserInput, timePerRoundUserInput, maxPlayersUserInput, roundsPerGameUserInput,1,chosenGames.size(),timePerRoundUserInput);
            tournamentFromDB = QueryIntermediate.createNewTournament(tournament);
            round = QueryIntermediate.getRoundFromTournament(tournamentFromDB.get_id(), GameWare.getInstance().getPlayer().getId(), tournamentFromDB.getCurrentRound());
            gsm.set(new ViewTournamentState(gsm,tournamentFromDB,round));
        } catch (IOException e) {
            e.printStackTrace();
            errorLabel.setText(IOExceptionText);
        }
    }

    private String getTournamentName(){
        return nameInputField.getText();
    }

    private int getTimePerRound(){
        return (int) timePerRoundSelectBox.getSelected();
    }

    private int getMaxPlayers(){
        return (int) maxPlayersSelectBox.getSelected();
    }

    private int getRoundsPerGame(){
        return (int) roundsPerGameSelectBox.getSelected();
    }


    private void removeKeyPadAtTouch(){
        stage.getRoot().addCaptureListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                if (!(event.getTarget() instanceof TextField)){
                    Gdx.input.setOnscreenKeyboardVisible(false);
                    stage.unfocusAll();
                }
                return false;
            }});
    }

    @Override
    protected void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)){
            handleBackBtnClick();
        }
    }

    @Override
    public void reset() {
        errorLabel.setText("");

        nameUserInput = "";
        timePerRoundUserInput = 1;
        maxPlayersUserInput = 2;
        roundsPerGameUserInput = 1;

        timePerRoundSelectBox.setSelected(timePerRoundUserInput);
        maxPlayersSelectBox.setSelected(maxPlayersUserInput);
        roundsPerGameSelectBox.setSelected(roundsPerGameUserInput);

        for(CheckBox cb : checkBoxes){
            cb.setChecked(false);
        }
    }
}