package com.gameware.game.states.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gameware.game.GameWare;
import com.gameware.game.QueryIntermediate;
import com.gameware.game.models.Game;
import com.gameware.game.models.Round;
import com.gameware.game.models.Tournament;
import com.gameware.game.sprites.LoadingText;
import com.gameware.game.GameStateManager;
import com.gameware.game.states.games.PlayStateUnion;

import java.io.IOException;
import java.util.List;

/*
    State where players can view a specified tournament, from there they can play their round,
    leave, or go to TournamentHighScoreState.

    Patterns: Publish-Subscribe, Delegation

    Publish-Subscribe and delegation is used on the buttons. Where P-S is used by utilizing
    click listeners and delegation is used by the button to delegate what happens to this state.
*/

public class ViewTournamentState extends MenuStateUnion {

//    Data
    private Tournament tournament;
    private Round round;

//    Labels
    private Label disabledPlayBtnFeedback = new Label("Waiting for other players to finish", skin, "big");
    private Label titleLabel = new Label("N/A", skin, "big");
    private Label roundDeadline = new Label("N/A", skin, "big");

//    Texts
    private final String backBtnText = "Back";
    private final String leaveBtnText = "Leave";
    private final String playBtnText = "Play Round";
    private final String viewPointsBtnText = "View Points";
    private final String nrPlayersText = "Players in tournament:";
    private final String roundsLeftText = "Rounds left:";
    private final String leaveDialogText = "Are you sure want to\nleave ";

    private String gameName = "";

//    Variables
    private final float playBtnWidth = buttonWidth*1.4f;
    private final float playBtnHeight = buttonHeight*1.2f;
    private final float imageWidthAndHeigh = Gdx.graphics.getWidth()/4;

    private Dialog dialog;

//    Loading text
    private boolean backBtnClicked = false;
    private boolean viewPointsBtnClicked = false;
    private boolean leaveConfirmed = false;
    private LoadingText loadingText = new LoadingText();


    public ViewTournamentState(GameStateManager gsm, Tournament tournament, Round r) {
        super(gsm);
        try {
            this.tournament = QueryIntermediate.getSpecificTournament(tournament.get_id());
        }catch (IOException e){
            this.tournament = tournament;
        }

        try{
            this.round = QueryIntermediate.getRoundFromTournament(this.tournament.get_id(), GameWare.getInstance().getPlayer().getId(), this.tournament.getCurrentRound());
        }catch(IOException e){
            this.round = r;
        }

        titleLabel.setText(tournament.getName());
        roundDeadline.setText("Round deadline\n"+round.getDeadlineDate().toLocaleString());

        try{
            List<Game> games = GameWare.getInstance().getGames();
            for (Game g : games){
                if(g.getId().equals(r.getGameId())){
                    gameName = g.getName();
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        makeStage();
    }


    protected void makeStage(){
        Table rootTable = makeRootTable();

        titleLabel.setFontScale(titleFontBigScale);
        rootTable.add(titleLabel).expandY().top().colspan(2);
        rootTable.row();
        disabledPlayBtnFeedback.getColor().a = 0;
        rootTable.add(disabledPlayBtnFeedback).colspan(2);
        rootTable.row();
        rootTable.add(makePlayBtn()).size(playBtnWidth, playBtnHeight).colspan(2);
        rootTable.row();

        Table currentRoundTable = new Table();
        currentRoundTable.pad(padding);
        currentRoundTable.setBackground(backgroundTableBlueRounded);
        currentRoundTable.add(new Label("This round:\n\n"+gameName,skin)).space(spacingLittle);
        PlayStateUnion state = GameWare.getInstance().getGameIdToPlayState().get(round.getGameId());
        currentRoundTable.add(new Image(state.getThumbnail())).width(imageWidthAndHeigh).height(imageWidthAndHeigh).spaceBottom(spacingMedium).colspan(2);
        rootTable.add(currentRoundTable).maxHeight(Gdx.graphics.getHeight()/5).colspan(2);
        rootTable.row();

        rootTable.add(new Label(nrPlayersText+" "+tournament.getCurrentPlayers(), skin)).colspan(2);
        rootTable.row();
        rootTable.add(new Label(roundsLeftText+" "+((tournament.getRoundsPerGame()*tournament.getGames().size())-tournament.getCurrentRound()+1), skin)).colspan(2);
        rootTable.row();

        Table innerTable = new Table();
        TextButton leaveBtn = new TextButton(leaveBtnText, skin);
        leaveBtn.addListener(new LeaveClickListener());
        innerTable.add(leaveBtn).size(buttonWidth, buttonHeight).spaceRight(spacingLittle);
        innerTable.add(makeViewPointsBtn()).size(buttonWidth, buttonHeight);

        rootTable.add(innerTable).colspan(2);
        rootTable.row();
        rootTable.add(makeBackBtn()).expand().bottom().left();
        roundDeadline.setFontScale(0.8f * super.scaleRatio);
        rootTable.add(roundDeadline).expand().bottom().right();

        stage.addActor(rootTable);
        makeLeaveDialog();
    }

//    Make widgets methods
    private void makeLeaveDialog(){
        dialog = new Dialog("", skin, "dialog") {
            public void result(Object obj) {
                if(obj.equals(true)){
                    setLeaveConfirmed();
                } else{
                    makeLeaveDialog();
                }

            }
        };
        Label l = new Label(leaveDialogText+tournament.getName()+"?\n", skin);
        l.setAlignment(Align.center);
        dialog.text(l);
        dialog.button("Yes", true).pad(padding); //sends "true" as the result
        dialog.button("No", false).pad(padding);  //sends "false" as the result
    }

    private TextButton makeBackBtn(){
        TextButton backBtn = new TextButton(backBtnText, makeTextButtonStyle(backColor));
        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                setBackBtnClicked();
            }
        });
        return backBtn;
    }

    private TextButton makePlayBtn(){
        TextButton playBtn;
        if(round.isPlayed()){
            TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
            textButtonStyle.up = skin.newDrawable("button", new Color(0x207b20a0));
            textButtonStyle.down = skin.newDrawable("button", new Color(0x207b20a0));
            textButtonStyle.font = skin.getFont("font-big");
            textButtonStyle.fontColor = Color.LIGHT_GRAY;

            playBtn = new TextButton(playBtnText, textButtonStyle);
            playBtn.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent e, float x, float y){
                    disabledPlayBtnFeedback.getColor().a = 1;
                    disabledPlayBtnFeedback.addAction(Actions.sequence(
                            new DelayAction(2),
                            Actions.fadeOut(1)));
                }
            });
        }
        else{
            playBtn = new TextButton(playBtnText, makeTextButtonStyle(greenColor));
            playBtn.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent e, float x, float y){
                    handlePlayBtnClick();
                }
            });
        }
        return playBtn;
    }

    private TextButton makeViewPointsBtn(){
        TextButton viewPoints = new TextButton(viewPointsBtnText, skin);
        viewPoints.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                setViewPointsBtnClicked();
            }
        });
        return viewPoints;
    }


    @Override
    public void update(float dt){
        super.update(dt);

        // User pressed the back button
        if(this.backBtnClicked && this.loadingText.textIsRendering()){
            this.handleBackBtnClick();
            this.backBtnClicked = false;
        }

        // User pressed the view points button
        if(this.viewPointsBtnClicked && this.loadingText.textIsRendering()){
            this.handleViewPointsBtnClick();
            this.viewPointsBtnClicked = false;
        }

        // User confirmed to leave the tournament
        if(this.leaveConfirmed && this.loadingText.textIsRendering()){
            this.handleLeaveConfirmed();
            this.leaveConfirmed = false;
        }

        this.loadingText.update(dt);
    }

    @Override
    public void render(SpriteBatch sb){
        super.render(sb);
        this.loadingText.draw(sb);
    }

    private void setLeaveConfirmed(){
        this.leaveConfirmed = true;
        this.loadingText.setLoading();
    }

    private void setBackBtnClicked(){
        this.backBtnClicked = true;
        this.loadingText.setLoading();
    }

    private void setViewPointsBtnClicked(){
        this.viewPointsBtnClicked = true;
        this.loadingText.setLoading();
    }

//    Handle click methods
    private void handleLeaveConfirmed(){
        try {
            QueryIntermediate.leaveTournament(tournament.get_id(), GameWare.getInstance().getPlayer().getId());
            CreateJoinTournamentState state = new CreateJoinTournamentState(gsm);
            state.setFeedbackLabelText(state.leftTournamentText+tournament.getName());
            gsm.set(state);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    private void handleBackBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new CreateJoinTournamentState(gsm));
    }

    private void handlePlayBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        PlayStateUnion state = GameWare.getInstance().getGameIdToPlayState().get(round.getGameId());
        state.setTournament(tournament);
        state.setRound(round);
        gsm.set(state);
    }

    private void handleViewPointsBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new TournamentHighScoreState(gsm, tournament, round));
    }

    private void handleLeaveBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        dialog.show(stage);
    }


//    Listener
    public class LeaveClickListener extends ClickListener{

        public void clicked(InputEvent event, float x, float y) {
            handleLeaveBtnClick();
        };
    }

    @Override
    protected void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)){
            handleBackBtnClick();
        }
    }
}
