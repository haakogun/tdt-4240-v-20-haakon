package com.gameware.game.states.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.gameware.game.GameWare;
import com.gameware.game.GameStateManager;
import com.gameware.game.states.State;


/*
    The MenuStateUnion class is the Union of all menu states.

    It contains size variables, common menu widget methods, and abstract method makeStage.
    Contains default implementations of the abstract methods inherited from State.
    Uses the LibGDX Stage class, and delegates methods such as render and dispose.

    Patterns: Union, (State), (Delegation)
 */

public abstract class MenuStateUnion extends State {

    //    Font
    protected final float scaleRatio = Float.valueOf(Gdx.graphics.getWidth()) / 1080f;
    protected final float fontScale = 3f * scaleRatio;
    protected final float tinierTitleFontBigScale = 1.5f * scaleRatio;
    protected final float titleFontBigScale = 2.5f * scaleRatio;

    //    Spacing
    protected final float spacingLittle = Gdx.graphics.getHeight()/50;
    protected final float spacingMedium = Gdx.graphics.getHeight()/15;
    protected final float spacingLarge = spacingMedium*2;

    //    Padding
    protected final int rootTablePadding = Gdx.graphics.getWidth()/15;
    protected final int rootTablePaddingTop = Gdx.graphics.getWidth()/10;
    protected final int padding = 50;

    //    Width & Height
    protected final int buttonWidth = Gdx.graphics.getWidth()/3;
    protected final int buttonHeight = Gdx.graphics.getHeight()/12;
    protected final float questionMarkWidthAndHeight = Gdx.graphics.getWidth()/25;
    protected final float imageWidthAndHeigh = Gdx.graphics.getWidth()/4;

    //    Textures
    protected TextureRegionDrawable background = new TextureRegionDrawable(new TextureRegion(new Texture("state/bg1.jpg")));
    protected TextureRegionDrawable backgroundScore = new TextureRegionDrawable(new TextureRegion(new Texture("state/bg_score.jpg")));
    protected TextureRegionDrawable backgroundFinTourn = new TextureRegionDrawable(new TextureRegion(new Texture("state/bg1_finTourn.jpg")));
    protected TextureRegionDrawable backgroundTableBlueRounded = new TextureRegionDrawable(new TextureRegion(new Texture("state/tableBGRounded.png")));

    protected TextureRegionDrawable questionMark = new TextureRegionDrawable(new TextureRegion(new Texture("state/questionMarkIcon.png")));
    protected Color backColor = Color.VIOLET;
    protected Color greenColor = Color.GREEN;

    //    Sound Effects
    protected Sound checkBoxSound;

    //    Variables
    private boolean firstTimeRunningUpdate = true;


    public MenuStateUnion(GameStateManager gsm){
        super(gsm);

        // Scales the font according to the ratio between the screen width and the default 1080 width
        skin.getFont("font").getData().setScale(fontScale);
        skin.getFont("font-big").getData().setScale(this.scaleRatio);

//        Add sound effects
        this.checkBoxSound = Gdx.audio.newSound(Gdx.files.internal("sfx/check_box.ogg"));
    }

//    Override Methods
    @Override
    protected void handleInput(){}

    @Override
    public void update(float dt){
        if(firstTimeRunningUpdate) {
//            First time running update change input processor. Or else it won't work when states are stacked in the gsm
            Gdx.input.setInputProcessor(stage);
            Gdx.input.setCatchBackKey(true);
            firstTimeRunningUpdate = false;
        }
        this.handleInput();
        stage.act(dt);
    }

    @Override
    public void render(SpriteBatch sb){
        stage.draw();
    }

    @Override
    public void dispose(){
        stage.dispose();
    }

//    The default reset is empty since not all states has anything to reset
    @Override
    public void reset(){}

//    Abstract methods
    protected abstract void makeStage();


//    UI methods:

//    Makes default rootTable
    protected Table makeRootTable(TextureRegionDrawable background){
        Table rootTable = new Table();
        rootTable.setFillParent(true);
        rootTable.setBackground(background);
        rootTable.pad(rootTablePadding);
        rootTable.padTop(rootTablePaddingTop);
        rootTable.defaults().space(spacingLittle);
        return rootTable;
    }
//    Makes default rootTable
    protected Table makeRootTable(){
        return makeRootTable(background);
    }

    protected TextureRegionDrawable makeColorBackground(Color color){
        Pixmap bgPixmap = new Pixmap(1,1, Pixmap.Format.RGB565);
        bgPixmap.setColor(color);
        bgPixmap.fill();
        return new TextureRegionDrawable(new TextureRegion(new Texture(bgPixmap)));
    }

//    Dialog methods

    protected Table makeTableWithLabelAndQuestionIcon(Label label, final Dialog dialog){
        Table table = new Table();
        table.add(label).spaceRight(spacingLittle/2);
        Image questionMarkImage = new Image(questionMark);
        questionMarkImage.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent e, float x, float y){
                if(GameWare.getInstance().isSoundEffectsOn()){ checkBoxSound.play(); }
                dialog.show(stage);
            }
        });
        table.add(questionMarkImage).width(questionMarkWidthAndHeight).height(questionMarkWidthAndHeight).top();
        return table;
    }

    protected Dialog makeQuestionIconDialog(Label label){
        Dialog dialog = new Dialog("", skin, "dialog") {
            public void result(Object obj) { }
        };

        label.setAlignment(Align.center);
        label.setWrap(true);
        dialog.getContentTable().add(label).width(Gdx.graphics.getWidth()/1.5f).pad(dialog.getHeight()/5f).padBottom(0);

        dialog.button("Okay", true); //sends "true" as the result
        dialog.getButtonTable().pad(dialog.getHeight()/2f);

        return dialog;
    }

    protected TextButton.TextButtonStyle makeTextButtonStyle(Color background){
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = skin.newDrawable("button", background);
        textButtonStyle.down = skin.newDrawable("button-down", background);
        textButtonStyle.font = skin.getFont("font-big");
        textButtonStyle.fontColor = Color.WHITE;
        return textButtonStyle;
    }
}
