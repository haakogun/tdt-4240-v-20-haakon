package com.gameware.game.states.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gameware.game.GameWare;
import com.gameware.game.QueryIntermediate;
import com.gameware.game.models.Point;
import com.gameware.game.models.Round;
import com.gameware.game.models.Tournament;
import com.gameware.game.GameStateManager;

import java.util.ArrayList;
import java.util.List;

/*
    State where players can view the current scores in a tournament.

    Patterns: Publish-Subscribe, Delegation

    Publish-Subscribe and delegation is used on the buttons. Where P-S is used by utilizing
    click listeners and delegation is used by the button to delegate what happens to this state.
*/

public class TournamentHighScoreState extends MenuStateUnion {

//    Data
    private Tournament tournament;
    private Round round;
    private List<Point> points = new ArrayList<>();

//    Labels
    private Label titleLabel = new Label("N/A", skin, "big");
    private final Label subHeadLabel = new Label("Tournament Points", skin, "big");
    private final Label colOneLabel = new Label("Player", skin, "big");
    private final Label colTwoLabel = new Label("Total", skin, "big");
    private final Label colThreeLabel = new Label("Last Round", skin, "big");
    private final Label yourScoreThisRoundLabel = new Label("Your score this round: ", skin);
    private final Label totalDialogLabel = new Label("Total number of tournament points (TP). The number of TP you get, depends on how many players there are in the tournament. Therefore no TP will be given until all players are finished with their round.\n\nFor example if there are 3 players in a tournament, then the one with the highest score would get 30 TP and the player with the lowest score would get 10 TP.  ", skin);
    private final Label lastRoundLabel = new Label("Tournament points (TP) recieved in the last round of the tournament. TP depends on every players score, so no TP will be given until all players are finished with their round.", skin);

//    Texts
    private final String backBtnText = "Back";


//    Dialogs
    private Dialog totalDialog;
    private Dialog lastRoundDialog;


    public TournamentHighScoreState(GameStateManager gsm, Tournament tournament, Round r){
        super(gsm);
        this.tournament = tournament;
        this.round = r;
        try {
            titleLabel.setText(tournament.getName());
            points = QueryIntermediate.getTournamentPoints(tournament.get_id());
        }catch(Exception e){
            e.printStackTrace();
        }

        totalDialog = makeQuestionIconDialog(totalDialogLabel);
        lastRoundDialog = makeQuestionIconDialog(lastRoundLabel);

        makeStage();
    }

    protected void makeStage(){
        Table rootTable = makeRootTable();
        rootTable.defaults().expandY();

        titleLabel.setFontScale(titleFontBigScale);
        rootTable.add(titleLabel);
        rootTable.row();
        subHeadLabel.setFontScale(tinierTitleFontBigScale);
        rootTable.add(subHeadLabel).expandY().top();
        rootTable.row();
        rootTable.add(makeHighScoreTable()).maxHeight(Gdx.graphics.getHeight()/1.75f);
        rootTable.row();

        Table yourScoreTable = new Table();
        yourScoreTable.pad(spacingLittle);
        yourScoreTable.setBackground(backgroundTableBlueRounded);
        yourScoreTable.add(yourScoreThisRoundLabel);
        yourScoreTable.add(new Label(((int)round.getScoreValue())+"", skin));
        rootTable.add(yourScoreTable).height(Gdx.graphics.getHeight()/10);
        rootTable.row();

        rootTable.add(makeBackBtn()).expand().bottom().left();

        stage.addActor(rootTable);
    }

//    Make widgets methods
    private Table makeHighScoreTable(){
        Table innerTable = new Table();
        innerTable.pad(padding);
        innerTable.defaults().space(spacingLittle).expand();
        innerTable.setBackground(backgroundTableBlueRounded);

        innerTable.add(colOneLabel).top();
        innerTable.add(makeTableWithLabelAndQuestionIcon(colTwoLabel,totalDialog)).top();
        innerTable.add(makeTableWithLabelAndQuestionIcon(colThreeLabel,lastRoundDialog)).top();
        innerTable.row();

        for(Point point : points){
            innerTable.add(new Label(point.getName(), skin));
            innerTable.add(new Label(point.getTotalPoints()+"", skin));
            innerTable.add(new Label(point.getLatestPoints()+"", skin));
            innerTable.row();
        }
        return innerTable;
    }

    private TextButton makeBackBtn(){
        TextButton backBtn = new TextButton(backBtnText, makeTextButtonStyle(backColor));
        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y){
                handleBackBtnClick();
            }
        });
        return backBtn;
    }

//    Handle click methods
    private void handleBackBtnClick(){
        if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
        gsm.set(new ViewTournamentState(gsm, tournament, round));
    }


    @Override
    protected void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)){
            handleBackBtnClick();
        }
    }
}
