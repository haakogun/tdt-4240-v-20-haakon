package com.gameware.game.states.games;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gameware.game.sprites.colorRushSprites.ColorRushButton;
import com.gameware.game.sprites.colorRushSprites.ColorRushTarget;
import com.gameware.game.GameStateManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ColorRushState extends PlayStateUnion {

    private List<ColorRushTarget> activeTargets;
    private List<ColorRushTarget> previousTargets;
    private List<ColorRushButton> buttons;
    private List<Texture> colorTextures;
    private Texture disabledColorTexture;
    private Texture background;
    private float disabledDuration = 0f;
    private float penaltyDuration = 1f;
    private int targetsHit = 0;
    private boolean buttonsEnabled = true;
    private boolean disposeTargetLeft = true;

    public ColorRushState(GameStateManager gsm){
        super(gsm);
        super.setThumbnail(new Texture(Gdx.files.internal("gameTextures/ColorRush/colorRushPhotoEdit.png")));
        super.setTotalTimeLimit(30f);

        // Creates the background
        this.background = new Texture(Gdx.files.internal("gameTextures/ColorRush/ColorRushBackground.jpg"));

        // Creates the color textures
        this.colorTextures = new ArrayList<Texture>();
        this.colorTextures.add(new Texture(Gdx.files.internal("gameTextures/ColorRush/RedSquare.png")));
        this.colorTextures.add(new Texture(Gdx.files.internal("gameTextures/ColorRush/BlueSquare.png")));
        this.colorTextures.add(new Texture(Gdx.files.internal("gameTextures/ColorRush/GreenSquare.png")));
        this.colorTextures.add(new Texture(Gdx.files.internal("gameTextures/ColorRush/VioletSquare.png")));

        this.disabledColorTexture = new Texture(Gdx.files.internal("gameTextures/ColorRush/GreySquare.png"));

        // Randomizes the button arrangement
        Collections.shuffle(this.colorTextures);

        activeTargets = new ArrayList<ColorRushTarget>();
        previousTargets = new ArrayList<ColorRushTarget>();
        buttons = new ArrayList<ColorRushButton>();

        // The four buttons used
        buttons.add(new ColorRushButton(0, 0,Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/8, this.colorTextures.get(0), this.disabledColorTexture, 0));
        buttons.add(new ColorRushButton(Gdx.graphics.getWidth()/2, 0,Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/8, this.colorTextures.get(1), this.disabledColorTexture, 1));
        buttons.add(new ColorRushButton(0, Gdx.graphics.getHeight()/8,Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/8, this.colorTextures.get(2), this.disabledColorTexture, 2));
        buttons.add(new ColorRushButton(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/8,Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/8, this.colorTextures.get(3), this.disabledColorTexture, 3));


        // Parameters for the targets
        int targetWidth = Gdx.graphics.getWidth()/8;
        int targetHeight = Gdx.graphics.getWidth()/8;
        int targetX = Gdx.graphics.getWidth()/2 - targetWidth/2;
        int targetY = Gdx.graphics.getHeight()/3;
        int prevColor = -1;
        int targetAmount = 7;

        // Creates the targets
        for(int i = 0; i<targetAmount; i++){
            int color = (int) (Math.floor(Math.random()*this.colorTextures.size()));

            // No repeating colors
            while(prevColor == color){
                color = (int) (Math.floor(Math.random()*this.colorTextures.size()));
            }

            prevColor = color;

            activeTargets.add(new ColorRushTarget(targetX, targetY, targetWidth, targetHeight, this.colorTextures.get(color), color, this.disposeTargetLeft));
            this.disposeTargetLeft = !this.disposeTargetLeft;
            targetY += targetHeight;
        }
    }

    @Override
    protected void handleInput() {
        if(Gdx.input.justTouched()){
            // Checks if any of the color buttons were pressed
            for(ColorRushButton button : this.buttons){
                if(button.isPressed(Gdx.input.getX(), Gdx.input.getY())){
                    this.colorChanged(button.getColorNum(),button);
                }
            }

            // Keeps score consistent
            this.setScore(this.targetsHit);
        }
    }

    @Override
    public void update(float dt) {
        // Effectively updates the pause button, the loading text, and the current duration (if the minigame is time-based)
        super.update(dt);

        // Updates targets
        for(ColorRushTarget target : this.activeTargets){
            target.update(dt);
        }

        for(ColorRushTarget target : this.previousTargets){
            target.update(dt);
        }

        // Updates buttons
        for(ColorRushButton button : this.buttons){
            button.update(dt);
        }

        if(this.previousTargets.size() > 0 && this.previousTargets.get(0).shouldDispose()){
            this.previousTargets.remove(0).dispose();
        }

        // Accepts input if the buttons are enabled
        if(this.buttonsEnabled) {
            this.handleInput();
        }

        // If the buttons are disabled after a previous failed press
        else{
            // Increases the counter for how long the buttons have been disabled so far
            this.disabledDuration += dt;

            // The buttons get enabled after being disabled for 1 second
            if(this.disabledDuration >= this.penaltyDuration){
                this.disabledDuration = 0f;
                this.buttonsEnabled = true;
                for(ColorRushButton button : this.buttons){
                    button.setEnabledColor();
                }
            }
        }

    }

    @Override
    public void render(SpriteBatch sb) {
        sb.begin();

        // Background
        sb.draw(this.background, 0, Gdx.graphics.getHeight()/5, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        sb.end();

        for (ColorRushButton button : this.buttons) {
            button.draw(sb);
        }

        for (ColorRushTarget target : this.activeTargets) {
            target.draw(sb);
        }

        for (ColorRushTarget target : this.previousTargets) {
            target.draw(sb);
        }

        //Effectively renders the pause button, the loading text, the score text and the current duration text
        super.render(sb);
    }

    @Override
    public void dispose() {
        // Effectively disposes the pause button and the loading text
        super.dispose();

        for (ColorRushButton button : this.buttons) {
            button.dispose();
        }

        for (ColorRushTarget target : this.activeTargets) {
            target.dispose();
        }

        for (ColorRushTarget target : this.previousTargets) {
            target.dispose();
        }

        for(Texture tex : this.colorTextures){
            tex.dispose();
        }

        this.background.dispose();
        this.disabledColorTexture.dispose();
    }

    @Override
    public void reset() {
        super.setCurrentDuration(0f);
        this.disabledDuration = 0f;
        this.setScore(0);
        this.targetsHit = 0;
        this.activeTargets.clear();
        this.previousTargets.clear();

        //If the game is reset when the buttons are disabled, they are also reset
        this.disabledDuration = 0f;
        this.buttonsEnabled = true;
        for(ColorRushButton button : this.buttons){
            button.setEnabledColor();
        }

        int targetWidth = Gdx.graphics.getWidth()/8;
        int targetHeight = Gdx.graphics.getWidth()/8;
        int targetX = Gdx.graphics.getWidth()/2 - targetWidth/2;
        int targetY = Gdx.graphics.getHeight()/3;
        int prevColor = -1;
        int targetAmount = 7;

        // Creates the targets
        for(int i = 0; i<targetAmount; i++){
            int color = (int) (Math.floor(Math.random()*this.colorTextures.size()));

            // No repeating colors
            while(prevColor == color){
                color = (int) (Math.floor(Math.random()*this.colorTextures.size()));
            }

            prevColor = color;

            activeTargets.add(new ColorRushTarget(targetX, targetY, targetWidth, targetHeight, this.colorTextures.get(color), color, this.disposeTargetLeft));
            this.disposeTargetLeft = !this.disposeTargetLeft;
            targetY += targetHeight;
        }
    }



    public void colorChanged(int color,ColorRushButton btn){

        // If the user pressed the correct button
        if(activeTargets.get(0).getColorNum() == color){

            // increases targets hit, used for score
            this.targetsHit++;

            //Plays the correct color sound
            btn.playSound(true);

            // Removes the bottom target
            ColorRushTarget targetHit = this.activeTargets.remove(0);
            targetHit.startDisposal();
            this.previousTargets.add(targetHit);


            // ------> Creates new target and places it at the top to keep the game going <-------
            ColorRushTarget topTarget = this.activeTargets.get(activeTargets.size() - 1);

            int newColor = (int) (Math.floor(Math.random()*this.colorTextures.size()));

            // No repeating colors
            while(topTarget.getColorNum() == newColor){
                newColor = (int) (Math.floor(Math.random()*this.colorTextures.size()));
            }

            ColorRushTarget newTarget = new ColorRushTarget((int) (topTarget.getPosition().x), Gdx.graphics.getHeight(), topTarget.getWidth(), topTarget.getHeight(), this.colorTextures.get(newColor), newColor, this.disposeTargetLeft);
            newTarget.setNextHeight(topTarget.getNextHeight() + topTarget.getHeight());
            activeTargets.add(newTarget);

            // Changes the direction the next target will be "disposed"
            this.disposeTargetLeft = !this.disposeTargetLeft;

            // <------ Creates new target and places it at the top to keep the game going ------->

            // Moves all the targets downward
            for(ColorRushTarget target : this.activeTargets){
                target.makeMove();
            }
        }

        //If the user pressed the incorrect button all the buttons get disabled
        else{
            this.buttonsEnabled = false;

            //Plays the wrong color sound
            btn.playSound(false);

            for(ColorRushButton button : this.buttons){
                button.setDisabledColor();
            }
        }
    }
}
