package com.gameware.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gameware.game.GameStateManager;
import com.gameware.game.GameWare;
import com.gameware.game.sprites.pauseStateSprites.ConfirmationBox;
import com.gameware.game.sprites.LoadingText;
import com.gameware.game.sprites.pauseStateSprites.PauseCircle;
import com.gameware.game.sprites.pauseStateSprites.PauseMenuButton;
import com.gameware.game.states.games.PlayStateUnion;

import java.util.ArrayList;
import java.util.List;

/*
    State shown when players pause a game.

    Since it is still inside a game, the group wanted it to have a different look than the other menu states.
    However it was not a game, so it doesn't extend either the MenuStateUnion or the PlayStateUnion.
    It is therefore a bit of a special state, kind of in between the menu states and the game states.
*/

public class PauseState extends State {

//    Data
    private PlayStateUnion pausedGame;

//    Textures
    private Texture background;
    private Texture pauseText;
    private Texture dimmingTexture;
    private Texture countdownNumberTexture;
    private List<Texture> countdownNumbers;

//    Variables
    private Color originalFontColor;
    private float countdownDurationLeft;
    private boolean countdownStarted;
    private boolean userExited;
    private boolean needsConfirmation;

//    Widgets
    private List<PauseCircle> pauseBackgroundCircles;
    private ConfirmationBox confirmationBox;
    private PauseMenuButton resumeButton;
    private PauseMenuButton exitButton;
    private LoadingText loadingText;


    public PauseState(GameStateManager gsm, PlayStateUnion pausedGame) {
        super(gsm);

        this.background = new Texture(Gdx.files.internal("pause/PauseBackground.jpg"));
        this.pauseText = new Texture(Gdx.files.internal("pause/PauseText.png"));
        this.dimmingTexture = new Texture(Gdx.files.internal("pause/DimmingTexture.png"));

        this.countdownNumbers = new ArrayList<Texture>();
        this.countdownNumbers.add(new Texture(Gdx.files.internal("pause/Number1.png")));
        this.countdownNumbers.add(new Texture(Gdx.files.internal("pause/Number2.png")));
        this.countdownNumbers.add(new Texture(Gdx.files.internal("pause/Number3.png")));
        this.countdownDurationLeft = 3f;
        this.countdownStarted = false;

        this.pauseBackgroundCircles = new ArrayList<PauseCircle>();

        this.pausedGame = pausedGame;
        this.originalFontColor = new Color(this.pausedGame.getFontColor());

        this.pausedGame.setFontColorWhite();

        this.loadingText = new LoadingText();
        this.userExited = false;

        int confirmationBoxWidth = Gdx.graphics.getWidth()*7/8;
        int confirmationBoxHeight = confirmationBoxWidth/2;
        this.confirmationBox = new ConfirmationBox(Gdx.graphics.getWidth()/2 - confirmationBoxWidth/2, Gdx.graphics.getHeight()/2 - confirmationBoxHeight/2, confirmationBoxWidth, confirmationBoxHeight);
        this.needsConfirmation = false;

        int buttonWidth = Gdx.graphics.getWidth()/3;
        int buttonHeight = buttonWidth/2;
        this.resumeButton = new PauseMenuButton(new Texture(Gdx.files.internal("pause/ResumeButton.png")), buttonWidth/3, Gdx.graphics.getHeight()/7, buttonWidth, buttonHeight);
        this.exitButton = new PauseMenuButton(new Texture(Gdx.files.internal("pause/ExitButton.png")), buttonWidth + buttonWidth*2/3, Gdx.graphics.getHeight()/7, buttonWidth, buttonHeight);


        for(int i = 0; i<25; i++){
            this.pauseBackgroundCircles.add(new PauseCircle());
        }
    }

    @Override
    protected void handleInput() {
        if(Gdx.input.justTouched() && !this.countdownStarted){
            int touchX = Gdx.input.getX();
            int touchY = Gdx.graphics.getHeight() - Gdx.input.getY();

            if(this.needsConfirmation) {
                // User doesn't want to exit after all
                if(this.confirmationBox.noPressed(touchX, touchY)) {
                    if(GameWare.getInstance().isSoundEffectsOn()){ pauseStateBtnSound.play(); }
                    this.needsConfirmation = false;
                }

                // User confirms the exit, this will now render the loading text and then post the
                // score and exit
                if(this.confirmationBox.yesPressed(touchX, touchY)) {
                    if(GameWare.getInstance().isSoundEffectsOn()){ buttonPressSound.play(); }
                    this.loadingText.setLoading();
                    this.userExited = true;
                }
            }

            else {
                // Starts the countdown for the resumal of the game
                if(this.resumeButton.isPressed(touchX, touchY)){
                    if(GameWare.getInstance().isSoundEffectsOn()){ pauseResumeBtnSound.play(); }

                    // Starts the countdown
                    this.countdownStarted = true;
                }

                // First step of exiting; user now needs to confirm the exit via the popup
                if(this.exitButton.isPressed(touchX, touchY)){
                    if(GameWare.getInstance().isSoundEffectsOn()){ pauseStateBtnSound.play(); }
                    this.needsConfirmation = true;
                }
            }
        }
    }

    @Override
    public void update(float dt) {
        if(this.userExited && this.loadingText.textIsRendering()){

            // Resets the font-color of the game to its original color
            this.pausedGame.setFontColorCustom(this.originalFontColor);

            this.gsm.pop();
            this.pausedGame.gameDone();
        }
        this.loadingText.update(dt);

        this.handleInput();

        // If the user has pressed the resume button the countdown has started
        if(this.countdownStarted){
            this.countdownDurationLeft -= dt;
            this.countdownNumberTexture = this.countdownNumbers.get(Math.min(Math.max((int) (this.countdownDurationLeft),0), 2));

            // If the countdown has finished we resume the game
            if(this.countdownDurationLeft <= 0){
                // Resets the font-color of the game to its original color
                this.pausedGame.setFontColorCustom(this.originalFontColor);

                this.gsm.pop();
            }
        }

        for(PauseCircle pc : this.pauseBackgroundCircles){
            pc.update(dt);
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.begin();

        // Background
        sb.draw(this.background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        sb.end();


        // Animated circles
        for(PauseCircle pc : pauseBackgroundCircles){
            pc.draw(sb);
        }

        this.pausedGame.renderText(sb);

        // Draws the two buttons (resume and exit)
        this.resumeButton.draw(sb);
        this.exitButton.draw(sb);

        int textWidth = Gdx.graphics.getWidth()/2;
        int textHeight = textWidth/15*4;

        sb.begin();

        // Pause text
        sb.draw(this.pauseText, Gdx.graphics.getWidth()/2 - textWidth/2 , Gdx.graphics.getHeight()*7/10, textWidth, textHeight);

        // Dimming layer that dims everything in the background when confirmation is needed or countdown has started
        if(this.needsConfirmation || this.countdownStarted) {
            sb.draw(this.dimmingTexture, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        }

        // If the countdown has started and the number texture is ready, render the countdown number
        if(this.countdownStarted && this.countdownNumberTexture != null) {
            sb.draw(this.countdownNumberTexture, Gdx.graphics.getWidth()/2 - Gdx.graphics.getWidth()/5/2, Gdx.graphics.getHeight()/1.85f,Gdx.graphics.getWidth()/5, Gdx.graphics.getWidth()/5);
        }

        sb.end();

        // Needs confirmation if the user has pressed the exit button
        if(this.needsConfirmation){
            this.confirmationBox.draw(sb);
        }

        this.loadingText.draw(sb);
    }

    @Override
    public void dispose() {
        for(PauseCircle pc : pauseBackgroundCircles){
            pc.dispose();
        }

        for(Texture texture : this.countdownNumbers){
            texture.dispose();
        }

        this.background.dispose();
        this.pauseText.dispose();
        this.resumeButton.dispose();
        this.exitButton.dispose();
        this.confirmationBox.dispose();
        this.dimmingTexture.dispose();
        this.loadingText.dispose();
    }

    @Override
    public void reset() {
        this.needsConfirmation = false;
        this.countdownStarted = false;
        this.countdownDurationLeft = 3f;
    }
}
