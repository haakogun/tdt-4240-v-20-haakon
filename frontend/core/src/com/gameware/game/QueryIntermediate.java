package com.gameware.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.Net.HttpMethods;
import com.badlogic.gdx.Net.HttpRequest;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.net.HttpParametersUtils;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import com.gameware.game.models.Alert;
import com.gameware.game.models.Game;
import com.gameware.game.models.Highscore;
import com.gameware.game.models.ModelInterface;
import com.gameware.game.models.Player;
import com.gameware.game.models.Point;
import com.gameware.game.models.Round;
import com.gameware.game.models.RoundCheck;
import com.gameware.game.models.Tournament;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TimeZone;

/*
    QueryIntermediate works as connector between frontend and backend. It takes care of all
    communications and requests with backend.

    Static class implemented with private constructor final class and only static methods.

    Patterns: Pipe and Filter, Client-Server
 */


public final class QueryIntermediate {

    private QueryIntermediate(){}

    private static String baseUrl = GameWare.apiBaseUrl;

    private static Json json = new Json();
    private static JsonReader jsonReader = new JsonReader();

    private static String[] sendGetRequest(String route) {
        HttpRequest request = new HttpRequest(HttpMethods.GET);
        request.setUrl(baseUrl + route);
        return sendRequest(request);
    }

    private static String[] sendPostRequest(String route, Map<String, String> params) {
        HttpRequest httpPost = new HttpRequest(HttpMethods.POST);
        httpPost.setUrl(baseUrl + route);
        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        httpPost.setContent(HttpParametersUtils.convertHttpParameters(params));
        return sendRequest(httpPost);
    }

    private static String[] sendPutRequest(String route, Map<String, String> params) {
        HttpRequest httpPut = new HttpRequest(HttpMethods.PUT);
        httpPut.setUrl(baseUrl + route);
        httpPut.setHeader("Content-Type", "application/x-www-form-urlencoded");
        if (params != null) {
            httpPut.setContent(HttpParametersUtils.convertHttpParameters(params));
        }
        return sendRequest(httpPut);
    }

    private static String[] sendDeleteRequest(String route) {
        HttpRequest httpDelete = new HttpRequest(HttpMethods.DELETE);
        httpDelete.setUrl(baseUrl + route);
        return sendRequest(httpDelete);
    }

    // Send a request to server and return response code and text
    private static String[] sendRequest(HttpRequest request) {
        final String[] response = new String[2];
        Gdx.net.sendHttpRequest(request, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(HttpResponse httpResponse) {
                response[0] = httpResponse.getStatus().getStatusCode() + "";
                response[1] = httpResponse.getResultAsString();
                // System.out.println(response);
            }

            @Override
            public void failed(Throwable t) {
                response[0] = "500";
                response[1] = "Failed";
                System.out.println("Something went wrong");
            }

            @Override
            public void cancelled() {
                response[0] = "500";
                response[1] = "Cancelled";
                System.out.println("Cancelled");
            }
        });
        while (response[1] == null) {
            try {
                Thread.sleep(50);
//                System.out.println("Sleeping");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    private static void checkStatusCode(String[] response) throws IOException {
        if (Integer.parseInt(response[0]) > 299) { // error
            throw new IOException(response[0] + ": " + response[1]);
        }
    }

    private static void checkObjectNotNull(Object object) throws NoSuchElementException {
        if (object == null) {
            throw new NoSuchElementException("No element found");
        }
    }

    // ---------------- Games methods ----------------
    public static Game getGameById(String gameId) throws IOException, NoSuchElementException {
        String route = "games/gamename/" + gameId;
        String[] response = sendGetRequest(route);
        checkStatusCode(response);
        Game game = json.fromJson(Game.class, response[1].substring(1, response[1].length() - 1));
        checkObjectNotNull(game);
        return game;
    }

    public static List<Game> getGames() throws IOException {
        String route = "games/";
        List<Game> games = new ArrayList<>();
        String[] response = sendGetRequest(route);
        checkStatusCode(response);
        JsonValue base = jsonReader.parse(response[1]);
        JsonValue.JsonIterator iterator = base.iterator();
        while (iterator.hasNext()) {
            Game game = json.fromJson(Game.class, iterator.next().toString());
            games.add(game);
        }
        return games;
    }

    // ---------------- Tournaments methods ----------------
    public static List<Tournament> getTournamentsForPlayer(String playerId, boolean active) throws IOException {
        String route = "tournament/player/" + playerId + "/" + active;
        List<Tournament> tournaments = new ArrayList<>();
        String[] response = sendGetRequest(route);
        checkStatusCode(response);
        JsonValue base = jsonReader.parse(response[1]);
        JsonValue.JsonIterator iterator = base.iterator();
        while (iterator.hasNext()) {
            Tournament tournament = json.fromJson(Tournament.class, iterator.next().toString());
            tournaments.add(tournament);
        }
        return tournaments;
    }

    public static Tournament getSpecificTournament(String tournamentID) throws IOException, NoSuchElementException {
        String route = "tournament/specific/" + tournamentID;
        String[] response = sendGetRequest(route);
        checkStatusCode(response);
        Tournament tournament = json.fromJson(Tournament.class, response[1]);
        checkObjectNotNull(tournament);
        return tournament;
    }

    public static Tournament joinATournament(String playerId) throws IOException, NoSuchElementException {
        String route = "tournament/join/";
        Map<String, String> params = new HashMap<>();
        params.put("playerId", playerId);
        String[] response = sendPostRequest(route, params);
        checkStatusCode(response);
        Tournament tournament = json.fromJson(Tournament.class, response[1]);
        checkObjectNotNull(tournament);
        return tournament;
    }

    public static int leaveTournament(String tournamentId, String playerId) throws IOException {
        String route = "tournament/leave";
        Map<String, String> params = new HashMap<>();
        params.put("tournamentid", tournamentId);
        params.put("playerid", playerId);
        String[] response = sendPostRequest(route, params);
        checkStatusCode(response);
        JsonValue base = jsonReader.parse(response[1]);
        return base.getInt("deletedRounds"); // number of deleted rounds in tournament
    }

    public static Tournament createNewTournament(Tournament tournament) throws IOException, NoSuchElementException {
        String route = "tournament/new";
        Map<String, String> params = new HashMap<>();
        params.put("playerId", tournament.getPlayers().get(0));
        params.put("games", tournament.getGames().toString());
        params.put("name", tournament.getName().trim());
        params.put("timePerRound", Double.toString(tournament.getTimePerRound()));
        params.put("maxPlayers", Integer.toString(tournament.getMaxPlayers()));
        params.put("roundsPerGame", Integer.toString(tournament.getRoundsPerGame()));
        params.put("startDelay", Integer.toString(tournament.getStartDelay()));
        String[] response = sendPostRequest(route, params);
        checkStatusCode(response);
        Tournament createdTournament = json.fromJson(Tournament.class, response[1]);
        checkObjectNotNull(tournament);
        return createdTournament;
    }

    public static List<Point> getTournamentPoints(String tournamentId) throws IOException {
        String route = "rounds/all/" + tournamentId;
        List<Point> points = new ArrayList<>();
        String[] response = sendGetRequest(route);
        checkStatusCode(response);
        JsonValue base = jsonReader.parse(response[1]);
        JsonValue.JsonIterator iterator = base.iterator();
        while (iterator.hasNext()) {
            Point point = json.fromJson(Point.class, iterator.next().toString());
            points.add(point);
        }
        return points;
    }

    public static RoundCheck doRoundCheck(String tournamentId) throws IOException, NoSuchElementException {
        String route = "tournament/roundcheck/" + tournamentId;
        String[] response = sendPutRequest(route, null);
        checkStatusCode(response);
        RoundCheck roundCheck = json.fromJson(RoundCheck.class, response[1]);
        checkObjectNotNull(roundCheck);
        return roundCheck;
    }

    // ---------------- Highscore methods ----------------
    public static List<Highscore> getHighscoresForGame(String gameId) throws IOException {
        String route = "highscores/gamescores/" + gameId;
        List<Highscore> highscores = new ArrayList<>();
        String[] response = sendGetRequest(route);
        checkStatusCode(response);
        JsonValue base = jsonReader.parse(response[1]);
        JsonValue.JsonIterator iterator = base.iterator();
        while (iterator.hasNext()) {
            Highscore highscore = json.fromJson(Highscore.class, iterator.next().toString());
            highscores.add(highscore);
        }
        return highscores;
    }

    public static Highscore getHighscoreForPlayerAndGame(String playerId, String gameId)
            throws IOException, NoSuchElementException {
        String route = "highscores/gamescore/" + playerId + "/" + gameId + "/";
        String[] response = sendGetRequest(route);
        checkStatusCode(response);
        Highscore highscore = json.fromJson(Highscore.class, response[1].substring(1, response[1].length() - 1));
        checkObjectNotNull(highscore);
        return highscore;
    }

    public static boolean postHighscore(String playerId, String gameId, int score)
            throws IOException, NoSuchElementException {
        String route = "highscores/highscore";
        Map<String, String> params = new HashMap<>();
        params.put("playerId", playerId);
        params.put("gameId", gameId);
        params.put("value", Integer.toString(score));
        String[] response = sendPostRequest(route, params);
        checkStatusCode(response);
        JsonValue base = jsonReader.parse(response[1]);
        return base.getBoolean("updated"); // true if new highscore
    }

    // ---------------- Round methods ----------------
    public static Round getRoundById(String roundId) throws IOException, NoSuchElementException {
        String route = "rounds/round/" + roundId;
        String[] response = sendGetRequest(route);
        checkStatusCode(response);
        Round round = json.fromJson(Round.class, response[1]);
        checkObjectNotNull(round);
        return round;
    }

    public static Round getRoundFromTournament(String tournamentId, String playerId, int roundNr)
            throws IOException, NoSuchElementException {
        String route = "rounds/specific/" + tournamentId + "/" + playerId + "/" + roundNr;
        String[] response = sendGetRequest(route);
        checkStatusCode(response);
        Round round = json.fromJson(Round.class, response[1]);
        checkObjectNotNull(round);
        return round;
    }

    public static List<ModelInterface> putRoundScore(String roundId, String tournamentId, int score) throws IOException, NoSuchElementException {
        String route = "rounds/" + roundId + "/" + tournamentId;
        Map<String, String> params = new HashMap<>();
        List<ModelInterface> models = new ArrayList<>();
        params.put("scoreValue", Integer.toString(score));
        String[] response = sendPutRequest(route, params);
        checkStatusCode(response);
        JsonValue base = jsonReader.parse(response[1]);

        String roundString = base.get("round").toString();
        String roundCheckString = base.get("roundCheck").toString();

        System.out.println(roundCheckString);
        System.out.println(roundCheckString.substring(roundCheckString.indexOf("{")));

        Round round = json.fromJson(Round.class, roundString.substring(roundString.indexOf("{")));
        RoundCheck roundCheck = json.fromJson(RoundCheck.class, roundCheckString.substring(roundCheckString.indexOf("{")));
        checkObjectNotNull(round);
        checkObjectNotNull(roundCheck);
        models.add(round);
        models.add(roundCheck);
        return models;
    }

    // ---------------- Player methods ----------------
    public static Player getPlayerById(String playerId) throws IOException, NoSuchElementException {
        String route = "players/username/" + playerId;
        String[] response = sendGetRequest(route);
        checkStatusCode(response);
        Player player = json.fromJson(Player.class, response[1].substring(1, response[1].length() - 1));
        checkObjectNotNull(player);
        return player;
    }

    public static Player loginPlayer(String username, String password) throws IOException, NoSuchElementException {
        username = username.trim();
        password = password.trim();
        String route = "players/login/" + username + "/" + password;
        String[] response = sendGetRequest(route);
        checkStatusCode(response);
        Player player = json.fromJson(Player.class, response[1].substring(1, response[1].length() - 1));
        checkObjectNotNull(player);
        return player;
    }

    public static Player createNewPlayer(String username, String password) throws IOException, NoSuchElementException {
        String route = "players/";
        username = username.trim();
        password = password.trim();
        Map<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);
        String[] response = sendPutRequest(route, params);
        checkStatusCode(response);
        Player player = json.fromJson(Player.class, response[1]);
        checkObjectNotNull(player);
        return player;
    }

    // ---------------- Alert methods ----------------
    public static List<Alert> getAlertsForPlayer(String playerId) throws IOException {
        String route = "alerts/" + playerId;
        List<Alert> alerts = new ArrayList<>();
        String[] response = sendGetRequest(route);
        checkStatusCode(response);
        JsonValue base = jsonReader.parse(response[1]);
        JsonValue.JsonIterator iterator = base.iterator();
        while (iterator.hasNext()) {
            Alert alert = json.fromJson(Alert.class, iterator.next().toString());
            alerts.add(alert);
        }
        return alerts;
    }

    public static int deleteAlertsForPlayer(String playerId) throws IOException {
        String route = "alerts/" + playerId;
        String[] response = sendDeleteRequest(route);
        checkStatusCode(response);
        JsonValue base = jsonReader.parse(response[1]);
        return base.getInt("deletedCount"); // number of alerts deleted
    }

    // ---------------- Model help methods ----------------
    public static Date convertToDate(String UTCDateString) {
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sourceFormat.setTimeZone(utc);
        try {
            return sourceFormat.parse(UTCDateString);
        } catch (ParseException e) {
            return null;
        }
    }
}
