package com.gameware.game.sprites;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

/*
    Abstract super class/union for game-sprites. Super class instead of interface because of
    necessary variables all sprites needs to have

    Patterns: Union, Update method
*/

public abstract class Sprite {

    protected Vector3 position;
    protected int width;
    protected int height;
    protected Object bounds;

    public abstract void reset();

    public abstract void draw(SpriteBatch sb);
    public abstract void update(float dt);
    public abstract void dispose();

}