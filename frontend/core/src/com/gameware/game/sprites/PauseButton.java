package com.gameware.game.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

import javax.xml.soap.Text;

// This is the pause button that is accessible in all minigames

public class PauseButton extends Sprite {
    private Texture blackButtonTexture;
    private Texture whiteButtonTexture;
    private Texture currentActiveTexture;


    // Customizable constructor
    public PauseButton(int x, int y, int width, int height, Texture blackBtnTex, Texture whiteBtnTex){
        this.position = new Vector3(x, y, 0);
        this.width = width;
        this.height = height;
        this.blackButtonTexture = blackBtnTex;
        this.whiteButtonTexture = whiteBtnTex;

        this.currentActiveTexture = blackButtonTexture;
    }

    // Constructor with default settings (black button)
    public PauseButton(){
        this(Gdx.graphics.getWidth() - Gdx.graphics.getWidth()/8,
                Gdx.graphics.getHeight() - Gdx.graphics.getHeight()/13,
                Gdx.graphics.getWidth()/10,Gdx.graphics.getWidth()/10,
                new Texture(Gdx.files.internal("pause/PauseButtonBlack.png")),
                new Texture(Gdx.files.internal("pause/PauseButtonWhite.png")));
    }


    @Override
    public void reset() {

    }

    @Override
    public void draw(SpriteBatch sb) {
        sb.begin();

        sb.draw(this.currentActiveTexture, this.position.x, this.position.y, this.width, this.height);

        sb.end();
    }

    @Override
    public void update(float dt) {
    }

    @Override
    public void dispose() {
        this.blackButtonTexture.dispose();
        this.whiteButtonTexture.dispose();
    }

    public boolean isPressed(int x, int y){
        return x > this.position.x && x < (this.position.x + this.width) && (Gdx.graphics.getHeight() - y) > this.position.y && (Gdx.graphics.getHeight() - y) < (this.position.y + this.height);
    }

    // The following two methods are used by the minigame-state to change the color of the pause button.
    // This can be done during playtime if any future games has a dynamic background and want to
    // alternate between using the two different colors on the pause button.

    public void setButtonWhite(){
        this.currentActiveTexture = this.whiteButtonTexture;
    }

    public void setButtonBlack(){
        this.currentActiveTexture = this.blackButtonTexture;
    }

    public void setPosition(int x, int y){
        this.position.x = x;
        this.position.y = y;
    }

    public void setWidth(int width){
        this.width = width;
    }

    public void setHeight(int height){
        this.height = height;
    }
}
