package com.gameware.game.sprites.fruitSlicerSprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.gameware.game.sprites.Sprite;

// This is the circle trail that are left when slicing in the Fruit Slicer minigame

public class SlicingCircle extends Sprite {
    private Texture slicingVFX;
    private int originalWidth;

    public SlicingCircle(int x, int y, Texture slicingVFX){
        this.position = new Vector3(x, y,0);
        this.slicingVFX = slicingVFX;
        this.width = Gdx.graphics.getWidth()/20;
        this.height = this.width;
        this.originalWidth = this.width;
    }

    @Override
    public void reset() {

    }

    @Override
    public void draw(SpriteBatch sb) {
        sb.begin();

        sb.draw(this.slicingVFX, this.position.x - this.width/2, this.position.y - this.height/2, this.width, this.height);

        sb.end();
    }

    @Override
    public void update(float dt) {
        this.width -= this.originalWidth*dt;
        this.height = this.width;
    }

    @Override
    public void dispose() {

    }

    public boolean isDisposable(){
        return this.width <= 0;
    }
}
