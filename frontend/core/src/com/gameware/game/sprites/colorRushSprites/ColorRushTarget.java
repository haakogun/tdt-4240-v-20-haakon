package com.gameware.game.sprites.colorRushSprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.gameware.game.sprites.Sprite;

// This is the block that you need to press the matching color to remove in the Color Rush minigame

public class ColorRushTarget extends Sprite {
     private int nextHeight;
    private Vector3 velocity;
    private Texture mainTexture;
    private int colorNum;
    private float disposeOffsetTime;
    private boolean disposalStarted;
    private boolean disposeAnimLeft;

    public ColorRushTarget(int x, int y, int width, int height, Texture mainTexture, int colorNum, boolean disposeAnimLeft){
        this.position = new Vector3(x, y, 0);
        this.nextHeight = y + height;
        this.velocity = new Vector3(0, (int) (-Gdx.graphics.getHeight()*0.2), 0);
        this.width = width;
        this.height = height;
        this.mainTexture = mainTexture;
        this.colorNum = colorNum;
        this.disposeOffsetTime = 0f;
        this.disposalStarted = false;
        this.disposeAnimLeft = disposeAnimLeft;
    }

    @Override
    public void reset() {

    }

    @Override
    public void draw(SpriteBatch sb) {
        sb.begin();

        sb.draw(this.mainTexture, this.position.x, this.position.y, this.width, this.height);

        sb.end();
    }

    @Override
    public void update(float dt) {

        // moves the target-rectangles downwards if the user successfully hit a target
       if((this.nextHeight - this.position.y) <= this.height) {
            this.velocity.scl(dt);
            this.position.add(0, velocity.y, 0);
            this.velocity.scl(1 / dt);
       }


       // Moves the targets to the side if it has been hit (i.e. disposalStarted = true)
        if(this.disposalStarted){
            this.velocity.scl(dt);
            this.position.add(velocity.x, velocity.y, 0);
            this.velocity.scl(1 / dt);

            // Increments the dispose offset time
            this.disposeOffsetTime += dt;
        }


       // Rounds the position to an integer
       position.x = (int) Math.floor(position.x);
       position.y = (int) Math.floor(position.y);
    }

    @Override
    public void dispose() {
        // Textures are shared so they should not be disposed
    }

    // Moves the target downwards, used for when the user hits a target and all other targets
    // should move down
    public void makeMove(){

        // Effectively sets where the target "should" be one target-height down
        this.nextHeight -= this.height;
    }

    public boolean isPressed(int x, int y){
        return x > this.position.x && x < (this.position.x + this.width) && (Gdx.graphics.getHeight() - y) > this.position.y && (Gdx.graphics.getHeight() - y) < (this.position.y + this.height);
    }

    public void startDisposal(){
        this.velocity.x = this.disposeAnimLeft ? -Gdx.graphics.getWidth()*2 : Gdx.graphics.getWidth()*2;
        this.disposalStarted = true;
    }

    public int getColorNum(){
        return this.colorNum;
    }

    public Vector3 getPosition(){
        return this.position;
    }

    public int getHeight(){
        return this.height;
    }

    public int getWidth(){
        return this.width;
    }

    public void setNextHeight(int nextHeight){
        this.nextHeight = nextHeight;
    }

    public int getNextHeight(){
        return this.nextHeight;
    }

    public boolean shouldDispose(){
        return this.disposeOffsetTime > 1;
    }
}
