package com.gameware.game.sprites.colorRushSprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.gameware.game.GameWare;
import com.gameware.game.sprites.Sprite;

// These are the buttons that needs to be pressed according to the matching color of the target in the Color Rush minigame

public class ColorRushButton extends Sprite {
    private Texture mainTexture;
    private Texture disabledTexture;
    private int colorNum;
    private boolean disabled;
    private Sound correct_sound;
    private Sound wrong_sound;

    public ColorRushButton(int x, int y, int width, int height, Texture mainTexture, Texture disabledTexture, int colorNum){
        this.position = new Vector3(x, y, 0);
        this.width = width;
        this.height = height;
        this.mainTexture = mainTexture;
        this.disabledTexture = disabledTexture;
        this.colorNum = colorNum;
        this.disabled = false;
        this.correct_sound = Gdx.audio.newSound(Gdx.files.internal("sfx/right_color.ogg"));
        this.wrong_sound = Gdx.audio.newSound(Gdx.files.internal("sfx/wrong_color.ogg"));
    }

    @Override
    public void reset() {
        this.disabled = false;
    }

    @Override
    public void draw(SpriteBatch sb) {

        Texture drawnTexture = this.mainTexture;

        sb.begin();

        if(this.disabled){
            drawnTexture = this.disabledTexture;
        }

        sb.draw(drawnTexture, this.position.x, this.position.y, this.width, this.height);

        sb.end();
    }

    @Override
    public void update(float dt) {
    }

    @Override
    public void dispose() {
        // Textures are shared so they should not be disposed
        wrong_sound.dispose();
        correct_sound.dispose();
    }

    public boolean isPressed(int x, int y){
        return x > this.position.x && x < (this.position.x + this.width) && (Gdx.graphics.getHeight() - y) > this.position.y && (Gdx.graphics.getHeight() - y) < (this.position.y + this.height);
    }

    public int getColorNum(){
        return this.colorNum;
    }

    // Effectively sets the color to grey
    public void setDisabledColor(){
        this.disabled = true;
    }

    // Effectively removes the grey color and sets the color to the main color
    public void setEnabledColor(){
        this.disabled = false;
    }

    public Vector3 getPosition(){
        return this.position;
    }

    public int getHeight(){
        return this.height;
    }

    public int getWidth(){
        return this.width;
    }

    public void playSound(Boolean correct){
        if(GameWare.getInstance().isSoundEffectsOn()){
            if(correct){ correct_sound.play(); }
            else{ wrong_sound.play(); }
        }

    }
}
