package com.gameware.game.sprites.pauseStateSprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.gameware.game.sprites.Sprite;

// This is the confirmation box when trying to exit a minigame when in the pause state

public class ConfirmationBox extends Sprite {
    private Texture boxTexture;

    public ConfirmationBox(int x, int y, int width, int height){
        this.position = new Vector3(x, y,0);
        this.width = width;
        this.height = height;

        this.boxTexture = new Texture(Gdx.files.internal("pause/ConfirmationSprite.png"));
    }

    @Override
    public void reset() {

    }

    @Override
    public void draw(SpriteBatch sb) {
        sb.begin();
        sb.draw(this.boxTexture, this.position.x, this.position.y, this.width, this.height);
        sb.end();
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void dispose() {
        this.boxTexture.dispose();
    }

    public boolean yesPressed(int xTouch, int yTouch){
        return xTouch > this.position.x && xTouch < (this.position.x + this.width/2) && yTouch > this.position.y && yTouch < (this.position.y + this.height/2);
    }

    public boolean noPressed(int xTouch, int yTouch){
        return xTouch > (this.position.x + this.width/2) && xTouch < (this.position.x + this.width) && yTouch > this.position.y && yTouch < (this.position.y + this.height/2);
    }

    public int getX(){
        return (int) this.position.x;
    }

    public int getY(){
        return (int) this.position.y;
    }

    public int getWidth(){
        return this.width;
    }

    public int getHeight(){
        return this.height;
    }
}
