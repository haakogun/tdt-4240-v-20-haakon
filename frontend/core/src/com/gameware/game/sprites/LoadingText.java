package com.gameware.game.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

// This is the "Loading..." text that can be seen throughout the application

public class LoadingText extends Sprite {
    private boolean isLoading = false;
    private boolean firstUpdateFinished = false;
    private Texture loadingText = new Texture(Gdx.files.internal("state/LoadingText.png"));
    private Texture dimmingBackgroundTexture = new Texture(Gdx.files.internal("state/DimmingTexture.png"));

    @Override
    public void reset() {
        this.firstUpdateFinished = false;
        this.isLoading = false;
    }

    @Override
    public void draw(SpriteBatch sb) {
        if(this.isLoading) {
            sb.begin();
            sb.draw(this.dimmingBackgroundTexture, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            sb.draw(this.loadingText, Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 2, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 10);
            sb.end();
        }
    }

    @Override
    public void update(float dt) {

        // If we've already completed the first call to this update method after isLoading was
        // set to true (this means that the draw method has been called) and the loading text is
        // visible we know that the loading has finished
        if(this.isLoading && this.firstUpdateFinished){
            this.isLoading = false;
            this.firstUpdateFinished = false;
        }

        // We need to wait one iteration of update() for the screen to render the loading text first
        else if(this.isLoading){
            this.firstUpdateFinished = true;
        }
    }

    @Override
    public void dispose() {
        this.dimmingBackgroundTexture.dispose();
        this.loadingText.dispose();
    }

    public void setLoading(){
        this.isLoading = true;
    }

    public boolean textIsRendering(){
        return this.firstUpdateFinished && this.isLoading;
    }
}
