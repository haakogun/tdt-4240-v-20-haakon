const express = require("express");
const router = express.Router();
const mongo = require("mongodb");
const MongoClient = mongo.MongoClient;
const connectionUrl = process.env.MONGO_CONNECTION_STRING;

router.get("/", (req, res) => {
  // Connect to database
  MongoClient.connect(
    connectionUrl,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
      // Unable to connect to database
      if (err) {
        res.sendStatus(500); // Internal server error
        return;
      }

      // Using the database gameWare and collection games
      const db = client.db("gameWare");
      const movieCollection = "games";

      db.collection(movieCollection)
        .find({})
        .toArray((err, result) => {
          if (err) {
            res.sendStatus(500);
            //console.log(err);
            return;
          }
          res.json(result);
          client.close();
        });
    }
  );
});

router.get("/gamename/:gameId", (req, res) => {
  // Connect to database
  MongoClient.connect(
    connectionUrl,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
      // Unable to connect to database
      if (err) {
        res.sendStatus(500); // Internal server error
        return;
      }

      // Using the database gameWare and collection games
      const db = client.db("gameWare");
      const collection = "games";
      let id;
      try {
        id = mongo.ObjectId(req.params.gameId);
      } catch (err) {
        res.status(400).send("Invalid id");
      }

      if (id == null) {
        res.status(400).send("Fields missing");
        return;
      }

      db.collection(collection)
        .find({
          _id: id
        })
        .toArray((err, result) => {
          if (err) {
            res.sendStatus(500);
            //console.log(err);
            return;
          }
          res.json(result);
          client.close();
        });
    }
  );
});

router.post("/", (req, res) => {
  MongoClient.connect(
    connectionUrl,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
      // Unable to connect to database
      if (err) {
        res.sendStatus(500); // Internal server error
        return;
      }

      const db = client.db("gameWare");
      const collection = "games";

      const name = req.body.name;
      const explanation = req.body.explanation;

      if (!name) {
        // Name not provided in body
        res.status(400).send("Name not provided");
        return;
      }

      if (!explanation) {
        res.status(400).send("Explanation not provided");
        return;
      }

      db.collection(collection).insertOne(
        { name, explanation },
        (err, result) => {
          if (err) {
            res.sendStatus(500); // Internal server error
            return;
          }
          res.json(result.ops[0]);
          client.close();
        }
      );
    }
  );
});

// Export API routes
module.exports = router;
