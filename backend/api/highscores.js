const express = require("express");
const router = express.Router();
const mongo = require("mongodb");
const MongoClient = mongo.MongoClient;
const connectionUrl = process.env.MONGO_CONNECTION_STRING;

router.get("/gamescore/:playerId/:gameId", (req, res) => {
  // Connect to database
  MongoClient.connect(
    connectionUrl,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
      // Unable to connect to database
      if (err) {
        res.sendStatus(500); // Internal server error
        return;
      }

      // Using the database gameWare and collection highscores
      const db = client.db("gameWare");
      const movieCollection = "highscores";

      let gameId, playerId;

      try {
        gameId = mongo.ObjectID(req.params.gameId);
        playerId = mongo.ObjectID(req.params.playerId);
      } catch (error) {
        res.status(400).send("Invalid IDs");
        return;
      }

      if (gameId == null || playerId == null) {
        res.status(400).send("Fields missing");
      }

      db.collection(movieCollection)
        .find({
          gameId: gameId,
          playerId: playerId,
        })
        .toArray((err, result) => {
          if (err) {
            res.sendStatus(500);
            return;
          }
          addNameToResult(result, db).then((objects) => {
            res.json(objects);
            client.close();
          });
        });
    }
  );
});

router.post("/highscore", (req, res) => {
  let gameId;
  let playerId;
  let value;
  try {
    gameId = mongo.ObjectID(req.body.gameId);
    playerId = mongo.ObjectID(req.body.playerId);
    value = !isNaN(parseInt(req.body.value)) ? parseInt(req.body.value) : null;
  } catch (err) {
    res.status(400).send("Invalid IDs");
    return;
  }
  if (isNaN(req.body.value)) {
    res.status(400).send("Invalid score");
    return;
  }

  if (gameId == null || playerId == null || value == null) {
    res.status(400).send("Missing fields");
    return;
  }

  MongoClient.connect(
    connectionUrl,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
      // Unable to connect to database
      if (err) {
        resolve(500); // Internal server error
      }

      const db = client.db("gameWare");
      const collection = "highscores";

      db.collection(collection).updateOne(
        {
          gameId: gameId,
          playerId: playerId,
        },
        { $max: { value: value } }, //Updates the score to the maximum of the old and new value
        { upsert: true }, // If the highscore does not exist it is created
        (err, result) => {
          if (err) {
            res.sendStatus(500);
            client.close();
            return;
          }
          res.json({
            updated: result.upsertedCount > 0 || result.modifiedCount > 0,
          });
          client.close();
        }
      );
    }
  );
});

router.get("/userscore/:playerId", (req, res) => {
  // Connect to database
  MongoClient.connect(
    connectionUrl,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
      // Unable to connect to database
      if (err) {
        res.sendStatus(500); // Internal server error
        return;
      }

      // Using the database gameWare and collection highscores
      const db = client.db("gameWare");
      const movieCollection = "highscores";

      let playerId;

      try {
        playerId = mongo.ObjectID(req.params.playerId);
      } catch (error) {
        res.status(400).send("Invalid IDs");
        return;
      }

      if (playerId == null) {
        res.status(400).send("Fields missing");
      }

      db.collection(movieCollection)
        .find({
          playerId: playerId,
        })
        .toArray((err, result) => {
          if (err) {
            res.sendStatus(500);
            //console.log(err);
            return;
          }
          res.json(result);
          client.close();
        });
    }
  );
});

router.get("/gamescores/:gameId", (req, res) => {
  // Connect to database
  MongoClient.connect(
    connectionUrl,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
      // Unable to connect to database
      if (err) {
        res.sendStatus(500); // Internal server error
        return;
      }

      // Using the database gameWare and collection highscores
      const db = client.db("gameWare");
      const collection = "highscores";

      let gameId;

      try {
        gameId = mongo.ObjectID(req.params.gameId);
      } catch (error) {
        res.status(400).send("Invalid IDs");
        return;
      }

      if (gameId == null) {
        res.status(400).send("Fields missing");
        return;
      }

      db.collection(collection)
        .find(
          { gameId: gameId }
          // gives the options to the query. We only want 5 scores and want them sorted by value in descending order
        )
        .sort({ value: -1 })
        .limit(5)
        .toArray((err, result) => {
          if (err) {
            res.sendStatus(500);
            return;
          }
          addNameToResult(result, db).then((objects) => {
            res.json(objects);
            client.close();
          });
        });
    }
  );
});

function addNameToResult(result, db) {
  return new Promise((resolve, reject) => {
    const playerIds = result.map((o) => o.playerId);
    db.collection("players")
      .find({
        _id: { $in: playerIds },
      })
      .project({ name: 1 })
      .toArray((err, nameresult) => {
        if (err) {
          res.sendStatus(500);
          //console.log(err);
          return;
        }
        nameresult = nameresult.map((doc) => {
          // rename _id => playerId
          doc.playerId = doc._id;
          delete doc._id;
          return doc;
        });
        let objects = [];
        for (let i = 0; i < result.length; i++) {
          objects.push({
            ...result[i],
            ...nameresult.find(
              (itmInner) =>
                String(itmInner.playerId) === String(result[i].playerId)
            ),
          });
        }
        resolve(objects);
      });
  });
}

// Export API routes
module.exports = router;
