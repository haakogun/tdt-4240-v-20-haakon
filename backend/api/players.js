const express = require("express");
const router = express.Router();
const mongo = require("mongodb");
const bcrypt = require("bcrypt");
const MongoClient = mongo.MongoClient;
const connectionUrl = process.env.MONGO_CONNECTION_STRING;
const saltRounds = 10;

router.get("/username/:playerId", (req, res) => {
  // Connect to database
  MongoClient.connect(
    connectionUrl,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
      // Unable to connect to database
      if (err) {
        res.sendStatus(500); // Internal server error
        return;
      }

      // Using the database gameWare and collection players
      const db = client.db("gameWare");
      const collection = "players";
      let id;
      try {
        id = mongo.ObjectId(req.params.playerId);
      } catch (err) {
        res.status(400).send("Invalid player id");
        return;
      }
      if (!id) {
        res.status(400).send("Player ID missing");
      }
      db.collection(collection)
        .find({
          _id: id,
        })
        .project({ name: 1 })
        // returns the username of the user
        .toArray((err, result) => {
          if (err) {
            res.sendStatus(500);
            return;
          }
          res.json(result);
          client.close();
        });
    }
  );
});

router.get("/login/:username/:password", (req, res) => {
  // Connect to database
  MongoClient.connect(
    connectionUrl,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
      // Unable to connect to database
      if (err) {
        res.sendStatus(500); // Internal server error
        return;
      }

      // Using the database gameWare and collection players
      const db = client.db("gameWare");
      const collection = "players";

      // Simply checks if there is a user that matches that exact username and password
      db.collection(collection)
        .find({
          name: req.params.username,
        })
        .toArray((err, result) => {
          if (err) {
            res.sendStatus(500);
            return;
          }
          // Compares the given password with the encrypted password stored in the database,
          // response is true on match, false else
          bcrypt.compare(
            req.params.password,
            result[0].password,
            (err, response) => {
              if (err) {
                res.sendStatus(500);
                client.close();
                return;
              }
              if (response) {
                res.json(result);
              } else {
                res.json([]);
              }
            }
          );
          client.close();
        });
    }
  );
});

router.put("/", (req, res) => {
  // Connect to database
  MongoClient.connect(
    connectionUrl,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
      // Unable to connect to database
      if (err) {
        res.sendStatus(500); // Internal server error
        return;
      }

      // Using the database gameWare and collection players
      const db = client.db("gameWare");
      const collection = "players";
      date = new Date();

      //Checks the parameters
      if (req.body.username == null || req.body.password == null) {
        res.status(400).send("Invalid parameters");
        return;
      }
      //Hashes the password
      bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
        if (err) {
          res.sendStatus(500); // Internal server error
          return;
        }
        // Inserts the user. Note that the name index is unique, inserting a user with an
        // already existing username will give an error.
        db.collection(collection).insertOne(
          {
            name: req.body.username,
            password: hash,
            dateJoined: date,
          },
          (err, result) => {
            if (err) {
              res.status(400).send("Already existing username");
              return;
            }
            res.json(result.ops[0]);
            client.close();
          }
        );
      });
    }
  );
});
// Export API routes
module.exports = router;
